﻿lauda
=====

.. currentmodule:: icicle.lauda





.. automodule:: icicle.lauda
   :ignore-module-all:

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~Lauda
   
   

   
   
   


















Lauda
-----

.. autoclass:: Lauda
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   





