﻿adc\_board
==========

.. currentmodule:: icicle.adc_board





.. automodule:: icicle.adc_board
   :ignore-module-all:

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~AdcBoard
   
   

   
   
   


















AdcBoard
--------

.. autoclass:: AdcBoard
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   





