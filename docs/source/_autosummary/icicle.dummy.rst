﻿dummy
=====

.. currentmodule:: icicle.dummy





.. automodule:: icicle.dummy
   :ignore-module-all:

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~DummyInstrument
   
   

   
   
   


















DummyInstrument
---------------

.. autoclass:: DummyInstrument
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   





