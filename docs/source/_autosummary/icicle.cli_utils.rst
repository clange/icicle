﻿cli\_utils
==========

.. currentmodule:: icicle.cli_utils





.. automodule:: icicle.cli_utils
   :ignore-module-all:

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      ~print_output
      ~verbosity
      ~with_instrument
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~InContextObjectField
      ~MonitoringLogger
   
   

   
   
   










print\_output
-------------

.. autofunction:: print_output

verbosity
---------

.. autofunction:: verbosity

with\_instrument
----------------

.. autofunction:: with_instrument












InContextObjectField
--------------------

.. autoclass:: InContextObjectField
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   

MonitoringLogger
----------------

.. autoclass:: MonitoringLogger
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   





