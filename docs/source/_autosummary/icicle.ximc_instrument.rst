﻿ximc\_instrument
================

.. currentmodule:: icicle.ximc_instrument





.. automodule:: icicle.ximc_instrument
   :ignore-module-all:

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~XimcInstrument
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      ~XimcCalibrationError
      ~XimcError
      ~XimcReadbackError
      ~XimcResultError
   
   


















XimcInstrument
--------------

.. autoclass:: XimcInstrument
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   








XimcCalibrationError
--------------------

.. autofunction:: XimcCalibrationError

XimcError
---------

.. autofunction:: XimcError

XimcReadbackError
-----------------

.. autofunction:: XimcReadbackError

XimcResultError
---------------

.. autofunction:: XimcResultError

