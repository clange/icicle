﻿relay\_board
============

.. currentmodule:: icicle.relay_board





.. automodule:: icicle.relay_board
   :ignore-module-all:

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~RelayBoard
      ~RelayBoardDOUBLE
      ~RelayBoardQUAD
      ~RelayBoardRD53A
   
   

   
   
   


















RelayBoard
----------

.. autoclass:: RelayBoard
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   

RelayBoardDOUBLE
----------------

.. autoclass:: RelayBoardDOUBLE
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   

RelayBoardQUAD
--------------

.. autoclass:: RelayBoardQUAD
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   

RelayBoardRD53A
---------------

.. autoclass:: RelayBoardRD53A
   :members:
   :undoc-members:
   :show-inheritance:

   
   .. automethod:: __init__

   
   

   
   
   





