ICICLE
======

**Instrument Control Interface and Commands Library developed @ETHZ**

*Vasilije Perovic, David Bacher, Simon Koch; ETH Zurich; 2020-2024*

ICICLE is a lightweight, resilient python library designed for provide instrument control and DCS functionality for lab setups and test beams. It consists of a set of interface classes for devices (and device types), and simple command line access to these interfaces.

For a quick start, read the :ref:`installation`, :ref:`connecting_instruments` and :ref:`graphical_monitoring` sections. For information on how to add your own instrument classes, see :ref:`implementing_instruments` and the :ref:`icicle_api_reference`.

Contributions are welcomed for new instrument or protocol implementations - please submit a merge request.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   connecting_instruments
   high_level_interfaces
   implementing_instruments
   graphical_monitoring
   testing

   api_reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
